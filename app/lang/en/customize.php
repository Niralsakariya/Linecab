<?php
return array(

    'Dashboard' => 'لوحة التحكم',
    'map_view' => 'وضع الخريطة',
    'Reviews' => 'التقييمات',
    'Information' => 'اضافة قائمة جديدة',
    'Types' => 'اضافة المركبات والأسعار',
    'Documents' => 'ملفات السائقين',
    'promo_codes' => 'العروض',
    'Customize' => 'Customize',
    'payment_details' => 'معلومات الدفع',
    'Settings' => 'الاعدادات',
    'Admin' => 'Admin',
    'admin_control' => 'Admin Control',
    'log_out' => 'تسجيل الخروج',
    'Provider' => 'السائقين',
    'User' => 'العملاء',
    'Taxi' => 'تاكسي',
    'Trip' => 'Service',
    'Walk' => 'Walk',
    'Request' => 'الطلبات',
);
